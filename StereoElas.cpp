#include "StereoElas.h"
#include "libelas/elas.h"

StereoElas::StereoElas (const std::string calib_filename) :
	StereoBase(calib_filename)
{
	std::cout << "Using stereo method: ELAS" << std::endl;
	Elas::parameters param(Elas::MIDDLEBURY);
	// ELAS parameters setting
	param.postprocess_only_left = false;
	param.filter_adaptive_mean = true;
	param.filter_median = false;
	param.filter_anisotropic = false;
	param.median_radius = 3;

	// 	param.subsampling = true;
	param.speckle_size =200;
	param.disp_max = 100;
	param.support_threshold = 0.98;
	param.grid_size = 5; // 5-10
	elas = new Elas(param);
}

StereoElas::~StereoElas()
{
	delete elas;
}

void StereoElas::calcPointCloud(cv::Mat &cld)
{
	cv::Mat gray_left, gray_right;
	if (scale_dwn != 1)
	{
		cv::remap(dwnleftImage, dwnrecLeftImage, dwnrmap[0][0], dwnrmap[0][1], cv::INTER_LINEAR);
		cv::remap(dwnrightImage, dwnrecRightImage, dwnrmap[1][0], dwnrmap[1][1], cv::INTER_LINEAR);
		
		cv::cvtColor(dwnrecLeftImage, gray_left, CV_BGR2GRAY);
		cv::cvtColor(dwnrecRightImage, gray_right, CV_BGR2GRAY);

		static int32_t dims[3] = {image_size_dwn.width, image_size_dwn.height, image_size_dwn.width};
		elas->process(gray_left.data, gray_right.data, (float*)disp_left.data, (float*)disp_right.data, dims);
		cv::blur(disp_left, rawDisparity, cv::Size(10,10));
		cv::reprojectImageTo3D(rawDisparity, cloud, dwnQ, true);
	}
	else
	{
		cv::remap(leftImage, recLeftImage, rmap[0][0], rmap[0][1], cv::INTER_LINEAR);
		cv::remap(rightImage, recRightImage, rmap[1][0], rmap[1][1], cv::INTER_LINEAR);

		cv::cvtColor(recLeftImage, gray_left, CV_BGR2GRAY);
		cv::cvtColor(recRightImage, gray_right, CV_BGR2GRAY);

		static int32_t dims[3] = {image_size.width, image_size.height, image_size.width};
		elas->process(gray_left.data, gray_right.data, (float*)disp_left.data, (float*)disp_right.data, dims);
		cv::blur(disp_left, rawDisparity, cv::Size(30,30));
		cv::reprojectImageTo3D(rawDisparity, cloud, Q, true);
	}
	cld = cloud;
}