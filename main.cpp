#include <string>
#include <opencv2/highgui/highgui.hpp>
#include "StereoElas.h"


int main(int argc, char** argv )
{
	std::string calib_filename = "abc.xml";
	
	cv::Mat left = cv::imread("left.png");
	cv::Mat right = cv::imread("right.png");
	cv::Mat cloud;
	StereoElas mStereo(calib_filename);
	mStereo.setScale(4);
	mStereo.setInputImages(left, right);
	mStereo.calcPointCloud(cloud);

}