#ifndef StereoElas_H
#define StereoElas_H

#include "StereoBase.h"

class Elas;

class StereoElas : public StereoBase
{
public:
	StereoElas(const std::string calib_filename);
	~StereoElas();

	void calcPointCloud(cv::Mat &cld);

protected:
private:
	Elas *elas;
};








#endif // StereoElas_H_ 
